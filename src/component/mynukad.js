import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './style1.css'
import {PostData,PostDataTATAapi} from './Api'
import swal from 'sweetalert';
import logoimg from './img/logo.png'
import tatalogoimg from './img/tataskylogo11.jpg'


export default class Mynukad extends Component {
    constructor(props) {
        super(props);
        this.state={
            "ID":"1000",
            mobile:"",
            Name:'',
            Email_Id:'',
            AparatmentName:'',
            UserMobile:''

        }
   
    }

    componentDidMount(){
       
       
    }
   
    validation = () => {console.log("validation")
        if ((this.state.mobile)||(this.state.mobile.length===0)) {
            let data=this.state.mobile.trim(" ")
            console.log("validation0")
            if(data.length===0){
                swal({
                    text: `Please enter number` ,
                })
            } else{console.log("validation1")
                if((data.length <10)||(!this.state.mobile.match(/^[0-9.]+$/))){
                    swal({
                        text: `Please enter valid Number` ,
                    })
                }
                else{
                    return true;
                }

            }
           
        }
        else{console.log("validation3")
           
        return true;
    }
        }



    submitHandler = e => {
            e.preventDefault()
            console.log(this.state)
         
            //var formdata = {  "ID":"1000"};
            const isValid = this.validation()
            if(isValid){
                const formdata={
                   // PhoneNbr:"9987820595"
                   PhoneNbr:this.state.mobile
                }
            PostData(formdata)
            .then((responseJson, error) => {
               
                if (responseJson.Status == "1") {
             
                this.setState({
                    UserName: responseJson.user_details[0].FirstName,
                    Email:responseJson.user_details[0].EmailID,
                    AparatmentName:responseJson.user_details[0].SocietyName,
                    UserMobile:responseJson.user_details[0].PhoneNbr,

                })

                this.state.mobile='';


                  console.log("hahahah",this.state.UserName,this.state.Email,this.state.AparatmentName,this.state.UserMobile)
                  swal({
                    text: `Thanks For showing your interest,We will connect soon` ,
                })
                }
                else
                {
                    this.refs.mob.value="";
                    swal({
                        text: `Mobile Number not Register ` ,
                    })

                }

                const postformdata=[
                    { "Attribute": "FirstName",
                      "Value":this.state.UserName
                       
                    },
                 
                      {
                         "Attribute": "EmailAddress",
                         "Value": this.state.Email
                      },
                      {
                          "Attribute": "Phone",
                          "Value": this.state.UserMobile
                      },
                      {
                          "Attribute": "mx_Society_Name",
                          "Value": this.state.AparatmentName
                      },
                      {
                        "Attribute": "Source",
                        "Value": "mynukad"
                    },
                    {
                        "Attribute": "mx_Broadband_City",
                        "Value": "Noida"
                    },
                    {
                        "Attribute": "mx_Broadband_State",
                        "Value": "Uttar Pradesh"
                    },
                    {
                        "Attribute": "mx_Pincode",
                        "Value": "201301"
                    },
                      {
                          "Attribute": "SearchBy",
                          "Value": "Phone"
                      }
                  ]
                     
               
                console.log("postdata",postformdata)

                PostDataTATAapi(postformdata)
                .then((responseJson, error) => {
                if (responseJson.Status == "Success") {
                     console.log("post data Success tataApi ")
                     this.refs.mob.value="";
                }
                else
                {
                    console.log("post data Error tataApi ")
                } })
            })



           
        }
        else {
            // swal({
            //     text: `Please enter number` ,
            // })
        }
        }

        manageChange = event => {
            this.setState({
   
                [event.target.name]: event.target.value
            })
        }
   

   
   
render(){
    const {mobile}=this.state
return(
    <div>
        <header>
    <div className="container">
        <div className="row">
            <div className="col">
                <img src={logoimg} className="Mlogoalign"/>
                {/* <h1 className="header" >mynukad</h1> */}
            </div>
        </div>
        <div className="row">
            <div className="col btn_alng">
            <div class="col-md-4">
            <div className="md-form mb-0">
            <label className="font-weight-bold">Please enter your registerd mobile number with mynukad</label>

                            <input type="text" id="name" onChange={this.manageChange} name="mobile" value={mobile} placeholder="Enter mobile number" ref="mob" class="form-control btn_alng" maxLength="12"/>
                        </div>
                        </div>
            <button type="button" onClick={this.submitHandler} class="btn btncolor btn_alng ">submit</button>
            <div>
            <label className="text" >Thanks for showing your interest for TataSky Broadband</label>
            </div>
            </div>

           
        </div>
        <div className="row">
            <div className="col">
                <div >
                <img  src={tatalogoimg} className="Tlogoalign" />
                </div>
                {/* <h1 className="header" >mynukad</h1> */}
            </div>
        </div>
    </div>
    </header>
    </div>
)
}
}
