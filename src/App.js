import React from 'react';
import './App.css';
import {Route, BrowserRouter as Router } from 'react-router-dom'
import mynukad from './component/mynukad'

function App() {
  return (
    <Router>
      <Route path="/" exact component={mynukad}/>
    </Router>
  );
}

export default App;
